Este projeto tem como objetivo servir como exemplo de protocolo de aplicação. Para tal, foi definido um protocolo de mensagens bem como foi implementado as aplicações cliente-servidor. Trata-se de uma aplicação implementada utilizando a linguagem de programação Java. 

### Sobre a aplicação
Trata-se de uma aplicação cliente-servidor na qual, o lado cliente é capaz de solicitar uma série de mensagens ao servidor obtendo deste a resposta adequada.  A aplicação apresenta tanto as funcionalidades do cliente quanto do servidor sendo que o modo de operação é definido quando o usuário executa a aplicação, informando no menu inicial. Se for um servidor, o servidor deverá iniciar um socket TCP servidor escutando conexões na porta 10000. Por outro lado, se for o cliente, deverá ser consultado o usuário sobre o endereço IP do servidor para abertura de conexão TCP cliente na porta 10000 do referido servidor.

### Sobre as funcionalidades do cliente
Do ponto de vista do usuário o cliente apresentará as seguintes funcionalidades:

* Calculadora (as quatro operações aritméticas)
* Impressão de mensagem de boas vindas
* Impressão da hora do sistema com contador do número de requisições recebidas

As funcionalidades acima listadas deverão ser implementadas de tal modo em que o cliente fará a requisição ao servidor, passando os parâmetros adequados, obterá a resposta e exibirá na tela. 

### Sobre o protocolo
O protocolo implementado contempla as seguintes funções base:
* ESTATISTICAS
* BOAS VINDAS
* CALCULADORA


Para cada uma das operações acima listadas o 

#### A operação ESTATISTICAS
O envio desta mensagem é disparado quando o usuário seleciona a opção imprimir estatísticas no menu apresentado. Neste sentido, a aplicação cliente deverá enviar a seguinte mensagem ao servidor (considere \n como sendo pula-linha):
    ESTATISTICA\n

Ao receber esta mensagem o servidor deverá obter a hora atual do sistema e retornar uma mensagem com o seguinte formato:

    OK\n
    Sat Oct 24 20:18:17 BRT 2020\n
    10\n

A mensagem acima tem a seguinte semântica:
* OK, indica que o processamento foi bem sucedido
* Sat Oct 24 20:18:17 BRT 2020, indica a data atual do servidor
* 10, indica o número de acessos realizados

Ao término do envio da mensagem o servidor encerrará a conexão automaticamente.

#### A operação BOAS VINDAS
O envio desta mensagem é disparado quando o usuário seleciona a opção boas vindas no menu apresentado. Neste sentido, a aplicação cliente deverá enviar a seguinte mensagem ao servidor (considere \n como sendo pula-linha):

    BOAS VINDAS\n
    João da Silva\n

Ao receber esta mensagem o servidor deverá processar a mensagem recebida e apresentar os seguintes comportamentos a depender da estrutura da mensagem:

* Se a mensagem recebida apresentar a estrutura prevista supracitada:
O servidor enviará a seguinte mensagem de retorno:

    OK\n
    Olá João da Silva, tudo bem?\n


* Se a mensagem recebida não apresentar a estrutura prevista supracida (não for informada a segunda linha)

    ERRO\n
    Estrutura incorreta da mensagem\n

Ao término do envio da mensagem o servidor encerrará a conexão automaticamente.
#### A operação CALCULADORA
O envio desta mensagem é disparado quando o usuário seleciona a opção calculadora no menu apresentado. Neste sentido, a aplicação cliente deverá solicitar dados adicionais para o usuário a depender do tipo de operação.

* Se for soma
Deverá ser pedido ao usuário uma lista de valores pretendidos onde será enviado a seguinte mensagem ao servidor

    CALCULADORA\n
    SOMA\n
    1\n
    20\n
    40\n
    \n


* Se for subtração, multiplicação e divisão
Deverá ser solicitado ao usuário dois valores referente aos operandos. O formato da mensagem seguirá os seguintes exemplos

#####Subtração

    CALCULADORA\n
    SUBTRACAO\n
    10\n
    20\n

#####Multiplicação

    CALCULADORA\n
    MULTIPLICACAO\n
    10\n
    20\n

#####Divisão

    CALCULADORA\n
    DIVISAO\n
    10\n
    20\n


**Processamento da mensagem no servidor**

#####SOMA

O servidor poderá responder seguindo os seguintes formatos

**Opção 1**
    OK\n
    10\n

**Opção 2**
    ERRO\n
    FALTA DE OPERANDOS\n

#####SUBTRAÇÃO

O servidor poderá responder seguindo os seguintes formatos

**Opção 1**

    OK\n
    10\n

**Opção 2**

    ERRO\n
    FALTA DE OPERANDOS\n

#####MULTIPLICAÇÃO

O servidor poderá responder seguindo os seguintes formatos

**Opção 1**

    OK\n
    10\n

**Opção 2**

    ERRO\n
    FALTA DE OPERANDOS\n

#####DIVISÃO

O servidor poderá responder seguindo os seguintes formatos

**Opção 1**

    OK\n
    10\n

**Opção 2**
    ERRO\n
    FALTA DE OPERANDOS\n

**Opção 3**

    ERRO\n
    DIVISAO POR ZERO\n
