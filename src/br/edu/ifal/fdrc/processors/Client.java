/**
 * 
 */
package br.edu.ifal.fdrc.processors;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import br.edu.ifal.fdrc.commands.Command;
import br.edu.ifal.fdrc.commands.client.BoasVindasCommand;
import br.edu.ifal.fdrc.commands.client.BoasVindasInvalidoCommand;
import br.edu.ifal.fdrc.commands.client.CalculadoraCommand;
import br.edu.ifal.fdrc.commands.client.CalculadoraInvalidoCommand;
import br.edu.ifal.fdrc.commands.client.EstatisticaCommand;
import br.edu.ifal.fdrc.commands.client.OpcaoInvalidaCommand;
import br.edu.ifal.fdrc.commands.client.SairCommand;

/**
 * @author ivocalado
 *
 */
public class Client {

	private Map<String, Command> commands;
	public Client() {
		commands = new HashMap<>();
		commands.put("1", new EstatisticaCommand());
		commands.put("2", new BoasVindasCommand());
		commands.put("3", new CalculadoraCommand());
		commands.put("4", new BoasVindasInvalidoCommand());
		commands.put("5", new CalculadoraInvalidoCommand());
		commands.put("6", new SairCommand());
	}
	public void run() {
		while(true) {
			printMenu();
			Scanner entrada = new Scanner(System.in);
			System.out.print("Opcao > ");
			String opt = entrada.nextLine();
			Command command = commands.getOrDefault(opt, new OpcaoInvalidaCommand());
			command.execute();
		}
		
	}
	
	private void printMenu() {
		System.out.println("\n\n|\tMenu de aplicação\t|");
		System.out.println("1 - Estatísticas");
		System.out.println("2 - Boas vindas");
		System.out.println("3 - Calculadora");
		System.out.println("4 - Simular Boas vindas incorreto");
		System.out.println("5 - Simular Calculadora incorreto");
		System.out.println("6 - Sair");
		System.out.println("|*******************************|");
	}

}
