package br.edu.ifal.fdrc.commands;

public interface Command {
	public void execute();
}
