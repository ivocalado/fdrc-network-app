package br.edu.ifal.fdrc.commands.client;

import br.edu.ifal.fdrc.commands.Command;

public class OpcaoInvalidaCommand implements Command {

	@Override
	public void execute() {
		System.out.println("Opção inválida selecionada. Tente novamente!!!");

	}

}
