package br.edu.ifal.fdrc.commands.client;

import br.edu.ifal.fdrc.commands.Command;

public class SairCommand implements Command {

	@Override
	public void execute() {
		System.out.println("Saindo da aplicação... Bye!!");
		System.exit(0);

	}

}
