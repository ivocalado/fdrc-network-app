package br.edu.ifal.fdrc.main;

import java.util.Scanner;

import br.edu.ifal.fdrc.processors.Client;
import br.edu.ifal.fdrc.processors.Server;

public class Main {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		System.out.print("Modo de execução [cliente | servidor]\n> ");
		String modo = entrada.nextLine();
		if(modo.equals("cliente")) {
			new Client().run();
		} else if(modo.equals("servidor")) {
			new Server().run();
		} else {
			System.out.println("Modo de execução incorreto.");
		}
	}
}
